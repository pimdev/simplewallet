//
//  CollectionViewLayoutHelpers.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import UIKit

protocol Diffable: Hashable {}

// MARK: Collection View Layout

extension UIViewController {
    func makeListLayout() -> UICollectionViewCompositionalLayout {
        // Match Parent Width
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .fractionalHeight(0.8)
        ))
        
        // Fixed Height (50)
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .absolute(75)
            ),
            subitems: [item]
        )
        
        return UICollectionViewCompositionalLayout {_,_ in
            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 10, bottom: 0, trailing: 10)
            return section
        }
    }
    
    func makeDynamicListLayout() -> UICollectionViewCompositionalLayout {

        // Match Parent Width
        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1),
            heightDimension: .estimated(60)
        ))
        item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: .fixed(5), trailing: nil, bottom: .fixed(5))
        
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .fractionalHeight(1)
            ),
            subitems: [item]
        )
        
        return UICollectionViewCompositionalLayout {_,_ in
            let section = NSCollectionLayoutSection(group: group)
            section.contentInsets = NSDirectionalEdgeInsets(top: 20, leading: 10, bottom: 0, trailing: 10)
            return section
        }
    }
}

extension UICollectionViewCell {
    func styleAsButton(color: UIColor = .systemBlue) {
        self.contentView.layer.cornerRadius = 10
        self.contentView.clipsToBounds = true
        self.contentView.backgroundColor = color
    }
}
