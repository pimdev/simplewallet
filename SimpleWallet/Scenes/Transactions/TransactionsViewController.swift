//
//  TransactionsViewController.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 05.02.2021.
//

import UIKit

typealias TransactionAddressRequest = Transactions.TransactionInfo.Request
typealias TransactionViewModel = Transactions.TransactionInfo.ViewModel.TransactionInfoViewModel

protocol TransactionsDisplayLogic: class {
    func displayTransactions(viewModel: Transactions.TransactionInfo.ViewModel)
    func displayError(message: String)
}

class TransactionsViewController: UIViewController {
    
    static var identifier: String {
        return "TransactionsViewController"
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var dataSource = makeDataSource()
    private var interactor: TransactionsInteractor?
    private var loadingIndicator: SpinnerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setup()
        // Testnet hardcoded address
        // TODO: Generate testnet addresses
        showLoadingIndicator()
        interactor?.loadTransactions(request: TransactionAddressRequest(address: "2Mu9i5SRDtuThPquH7SN3UeUYCgjJjeG4b3"))
    }
}

extension TransactionsViewController: TransactionsDisplayLogic {
    func displayTransactions(viewModel: Transactions.TransactionInfo.ViewModel) {
        hideLoadingIndicator()
        var snapshot = NSDiffableDataSourceSnapshot<Section, TransactionViewModel>()
        
        snapshot.appendSections([.main])
        snapshot.appendItems(viewModel.transactions, toSection: .main)

        dataSource.apply(snapshot)
    }
    func displayError(message: String) {
        print("Should create UI to display error message")
    }
}

extension TransactionsViewController {
    private func setup() {
        let viewController = self
        let interactor = TransactionsInteractor()
        let presenter = TransactionsPresenter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = viewController
    }
    
    private func setupCollectionView() {
        collectionView.collectionViewLayout = makeDynamicListLayout()
        collectionView.dataSource = makeDataSource()
        collectionView.register(TransactionCollectionViewCell.self, forCellWithReuseIdentifier: TransactionsViewController.cellReuseID)
    }
    
    private func showLoadingIndicator() {
        let loadingIndicator = SpinnerViewController()
        addChildAndView(child: loadingIndicator)
        self.loadingIndicator = loadingIndicator
    }
    
    private func hideLoadingIndicator() {
        if let loadingIndicator = loadingIndicator {
            hideChildAndView(child: loadingIndicator)
        }
    }
}

extension TransactionsViewController {
    private static let cellReuseID = "transaction-cell"
    
    private func makeDataSource() -> UICollectionViewDiffableDataSource<Section, TransactionViewModel> {
        UICollectionViewDiffableDataSource(
            collectionView: collectionView,
            cellProvider: { collectionView, indexPath, transactionInfo in
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Self.cellReuseID,
                    for: indexPath
                ) as! TransactionCollectionViewCell
                
                cell.transactionHashLabel.text = transactionInfo.transactionHashText
                cell.transactionAmountLabel.text = transactionInfo.transactionAmountText
                cell.styleAsButton()
                
                return cell
            }
        )
    }
}

extension UIViewController {
    func addChildAndView(child: UIViewController) {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func hideChildAndView(child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
}
