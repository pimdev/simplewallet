//
//  TransactionsCollectionViewCell.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 05.02.2021.
//

import UIKit

class TransactionCollectionViewCell: UICollectionViewCell {
    var transactionHashLabel: UILabel!
    var transactionAmountLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        let transactionHashLabel = UILabel()
        transactionHashLabel.numberOfLines = 0
        transactionHashLabel.lineBreakMode = .byWordWrapping
        transactionHashLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        contentView.addSubview(transactionHashLabel)
        
        transactionHashLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            transactionHashLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant:10),
            transactionHashLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant:10),
            transactionHashLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant:-10)
        ])
        self.transactionHashLabel = transactionHashLabel
        
        let transactionAmountLabel = UILabel()
        contentView.addSubview(transactionAmountLabel)
        
        transactionAmountLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            transactionAmountLabel.leadingAnchor.constraint(equalTo: transactionHashLabel.leadingAnchor),
            transactionAmountLabel.topAnchor.constraint(equalTo: transactionHashLabel.bottomAnchor, constant:10),
            transactionHashLabel.trailingAnchor.constraint(greaterThanOrEqualTo: contentView.trailingAnchor, constant:-10),
            transactionAmountLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant:-10),
        ])
        self.transactionAmountLabel = transactionAmountLabel
    }
}
