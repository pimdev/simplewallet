//
//  MnemonicViewModel.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 04.02.2021.
//

import Foundation


enum MnemonicData {
    struct MnemonicRequest {
        var value: String
    }
    struct MnemonicResponse {
        var index: Int
        var value: String
        var selected: Bool
    }
    struct MnemonicViewModel: Hashable, Equatable {
        static func == (lhs: MnemonicViewModel, rhs: MnemonicViewModel) -> Bool {
            lhs.index == rhs.index && lhs.selected == rhs.selected && lhs.value == rhs.value
        }
        
        var index: Int
        var value: String
        var selected: Bool
        
        init(index: Int, value: String, selected: Bool) {
            self.index = index
            self.value = value
            self.selected = selected
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(index)
            hasher.combine(value)
            hasher.combine(selected)
        }
    }
}

enum ConfirmationMnemonicData {
    struct ConfirmationMnemonicDataPassing {
        var value: String
    }
}
