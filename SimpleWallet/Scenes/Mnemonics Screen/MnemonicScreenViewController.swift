//
//  MnemonicScreenViewController.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 04.02.2021.
//

import UIKit

protocol MnemonicDisplayLogic: class {
    func displayMnemonic(viewModel: NSDiffableDataSourceSnapshot<Section, MnemonicData.MnemonicViewModel>)
    func displayValidationError(_ message: String)
    func displayValidationSuccess(_ message: String, actionName: String)
    func displayWalletCreationCompleted()
    
    func updateTitle(_ title: String)
    
    var dataPassing: [ConfirmationMnemonicData.ConfirmationMnemonicDataPassing]? { get set }
}

class MnemonicViewController: UIViewController {
    static var identifier: String {
      return "MnemonicViewController"
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    private lazy var dataSource = makeDataSource()
    
    var interactor: MnemonicInteractor?
    var createWalletRouter: (WalletRoutingLogic & WalletRoutingDataPassingLogic)?
    
    var dataPassing: [ConfirmationMnemonicData.ConfirmationMnemonicDataPassing]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        let viewController = self
        
        let interactor = MnemonicInteractor(data: dataPassing)
        let presenter = MnemonicPresenter()
        
        viewController.interactor = interactor
        interactor.presenter = presenter
        interactor.dataPassing = createWalletRouter
        presenter.viewController = viewController
        
        setupCollectionView()
        interactor.updateMnemonic()
    }
    
    func setupCollectionView() {
        collectionView.dataSource = makeDataSource()
        collectionView.delegate = self
        collectionView.collectionViewLayout = makeListLayout()
    }
}

extension MnemonicViewController {
    @IBAction func didPressValidateMnemonic(_ sender: Any) {
        interactor?.validateWalletInfo()
    }
}

extension MnemonicViewController: MnemonicDisplayLogic {
    func displayConfirmationForMnemonic(dataPassing: [ConfirmationMnemonicData.ConfirmationMnemonicDataPassing]) {
        
    }
    
    func displayMnemonic(viewModel: NSDiffableDataSourceSnapshot<Section, MnemonicData.MnemonicViewModel>) {
        dataSource.apply(viewModel)
    }
    
    func displayValidationError(_ message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func displayValidationSuccess(_ message: String, actionName: String) {
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "Wallet Name"
        }
        alert.addAction(UIAlertAction(title: actionName, style: .default, handler: { action in
            let textField = alert.textFields![0] as UITextField
            guard let text = textField.text , text.count > 0 else { return }
            self.interactor?.createWalletWithName(text)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func displayWalletCreationCompleted() {
        createWalletRouter?.showWalletCreationCompletion()
    }
    
    func updateTitle(_ title: String) {
        self.titleLabel.text = title
    }
}

// MARK: Collection View Delegate

extension MnemonicViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        interactor?.validateMnemonicAt(index: indexPath.row)
    }
}

// MARK: Collection View Data Source

extension MnemonicViewController {
    private static let cellReuseID = "mnemonic-cell"
    
    func makeDataSource() -> UICollectionViewDiffableDataSource<Section, MnemonicData.MnemonicViewModel> {
        UICollectionViewDiffableDataSource(
            collectionView: collectionView,
            cellProvider: { collectionView, indexPath, mnemonic in
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Self.cellReuseID,
                    for: indexPath
                ) as! MnemonicCollectionViewCell
                
                cell.textLabel.text = mnemonic.value
                let color: UIColor = mnemonic.selected ? .systemRed : .systemBlue
                cell.styleAsButton(color: color)
                
                return cell
            }
        )
    }
}



