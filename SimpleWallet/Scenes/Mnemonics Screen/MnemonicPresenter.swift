//
//  MnemonicPresenter.swift
//  SimpleWallet

import UIKit

protocol MnemonicPresentationLogic {
    func presentMnemonic(response: [MnemonicData.MnemonicResponse], indexed: Bool)
    func validateMnemonic(response: Bool)
    func finishWalletCreateion()
    func updateTitle(_ isValidating: Bool)
}

class MnemonicPresenter: MnemonicPresentationLogic {
    weak var viewController: MnemonicDisplayLogic?
        
    func presentMnemonic(response: [MnemonicData.MnemonicResponse], indexed: Bool) {
        if indexed {
            viewController?.displayMnemonic(viewModel: snapshotFromMnemonic(response.indexed()))
        } else {
            viewController?.displayMnemonic(viewModel: snapshotFromMnemonic(response))
        }
    }
    
    func validateMnemonic(response: Bool) {
        if !response {
            viewController?.displayValidationError("Selection Not Matching")
        } else {
            viewController?.displayValidationSuccess("Wallet Created", actionName: "Save")
        }
    }
    
    func finishWalletCreateion() {
        viewController?.displayWalletCreationCompleted()
    }
    
    func updateTitle(_ isValidating: Bool) {
        let title = !isValidating ? "Store Your Mnemonic Somewhere Safe" : "Select Your Mnemonic In The Right Order"
        viewController?.updateTitle(title)
    }
}

extension MnemonicPresenter {
    private func snapshotFromMnemonic(_ rsponse: [MnemonicData.MnemonicResponse]) -> NSDiffableDataSourceSnapshot<Section, MnemonicData.MnemonicViewModel> {
        var snapshot = NSDiffableDataSourceSnapshot<Section, MnemonicData.MnemonicViewModel>()
        snapshot.appendSections([.main])
        let mnemonicViewModel = rsponse.map { mnemonic in
            MnemonicData.MnemonicViewModel(index: mnemonic.index, value: mnemonic.value, selected: mnemonic.selected)
        }
        snapshot.appendItems(mnemonicViewModel, toSection: .main)
        return snapshot
    }
}

extension Array where Element == MnemonicData.MnemonicResponse {
    func indexed() -> [MnemonicData.MnemonicResponse] {
        var index = 0
        let indexedValues: [MnemonicData.MnemonicResponse] = self.map { value in
            index += 1
            return MnemonicData.MnemonicResponse(index: index, value: "\(index) \(value.value)", selected: value.selected)
        }
        return indexedValues
    }
}
