//
//  WalletRouter.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 04.02.2021.
//

import UIKit

protocol WalletRoutingDataPassingLogic {
    func passMnemonicConfirmationData(dataPassing: [ConfirmationMnemonicData.ConfirmationMnemonicDataPassing])
}

protocol WalletRoutingLogic {
    func showNewWalletMnemonicScreen()    
    func showWalletCreationCompletion()
}

class WalletRouter {
    weak var viewController: WalletViewController?
}

extension WalletRouter: WalletRoutingLogic, WalletRoutingDataPassingLogic {
    // MARK: Routing
    
    func showNewWalletMnemonicScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // force unwrap
        let mnemonicViewController = storyboard.instantiateViewController(identifier: MnemonicViewController.identifier) as! MnemonicViewController
        mnemonicViewController.createWalletRouter = self
        viewController?.navigationController?.pushViewController(mnemonicViewController, animated: true)
    }
    
    func showImportWalletScreen() {
        let importWalletViewController = ImportWalletViewController(nibName: nil, bundle: nil)
        importWalletViewController.router = self
        viewController?.navigationController?.present(importWalletViewController, animated: true, completion: nil)
    }
    
    func passMnemonicConfirmationData(dataPassing: [ConfirmationMnemonicData.ConfirmationMnemonicDataPassing]) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // force unwrap
        let mnemonicViewController = storyboard.instantiateViewController(identifier: MnemonicViewController.identifier) as! MnemonicViewController
        mnemonicViewController.createWalletRouter = self
        mnemonicViewController.dataPassing = dataPassing
        viewController?.navigationController?.pushViewController(mnemonicViewController, animated: true)
    }
    
    func showWalletCreationCompletion() {
        viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
