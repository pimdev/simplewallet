//
//  WalletViewController.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import UIKit

protocol WalletDisplayLogic: class {
    func displayWalletDescription(_ walletDescription: String)
    func displayMissingWalletMessage(_ message: String)
    
    func displayWalletAddress(_ address: String?)
}

class WalletViewController: UIViewController {
    static var identifier: String { return "WalletViewController" }
    
    @IBOutlet weak var walletInfoLabel: UILabel!
    
    var router: WalletRouter?
    var interactor: WalletInteractor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
}

extension WalletViewController {
    func setup() {
        setupNavigationbar()
        setupGestureRecognizers()
        
        let viewController = self
        
        let router = WalletRouter()
        let interactor = WalletInteractor()
        let presenter = WalletPresenter()
        
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        
        interactor.loadWallet()
    }
    
    func setupGestureRecognizers() {
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(copyAddress))
        doubleTapGesture.numberOfTapsRequired = 2
        walletInfoLabel.isUserInteractionEnabled = true
        walletInfoLabel.addGestureRecognizer(doubleTapGesture)
    }
    
    func setupNavigationbar() {
        let addWalletButton = UIBarButtonItem(systemItem: .add)
        addWalletButton.action = #selector(addWallet)
        addWalletButton.target = self
        navigationItem.rightBarButtonItem = addWalletButton
        
        let importWalletButton = UIBarButtonItem(systemItem: .search)
        importWalletButton.action = #selector(importWallet)
        importWalletButton.target = self
        navigationItem.rightBarButtonItems = [addWalletButton, importWalletButton]
    }
}

extension WalletViewController {
    @objc
    func addWallet() {
        router?.showNewWalletMnemonicScreen()
    }
    
    @objc
    func importWallet() {
        router?.showImportWalletScreen()
    }
    
    @objc
    func copyAddress() {
        interactor?.copyWalletAddress()
    }
}

extension WalletViewController: WalletDisplayLogic {
    func displayWalletDescription(_ walletDescription: String) {
        self.walletInfoLabel.text = walletDescription
    }
    
    func displayMissingWalletMessage(_ message: String) {
        self.walletInfoLabel.text = message
    }
    
    func displayWalletAddress(_ address: String?) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = address
    }
}
