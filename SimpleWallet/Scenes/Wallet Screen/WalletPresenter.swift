//
//  WalletPresenter.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

protocol WalletPresentationLogic {
    func presentWallet(_ wallet: WalletInfo?)
    func presentWalletAddress(_ address: String?)
}

class WalletPresenter {
    weak var viewController: WalletDisplayLogic?
}

extension WalletPresenter: WalletPresentationLogic {
    func presentWallet(_ wallet: WalletInfo?) {
        if let wallet = wallet, let address = wallet.address {
            viewController?.displayWalletDescription("Wallet: \(wallet.name)\nAddress: \(address)")
        } else {
            viewController?.displayMissingWalletMessage("No Wallet Available")
        }
    }
    
    func presentWalletAddress(_ address: String?) {
        viewController?.displayWalletAddress(address)
    }
}
