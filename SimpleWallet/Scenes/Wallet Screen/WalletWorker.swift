//
//  WalletWorker.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

class WalletWorker: WalletLogic {
    private let walletCore: WalletLogic = WalletCoreLogic()
    
    var walletAddress: String? {
        guard let wallet = getWallet() else { return nil }
        return createBTCAddressFromMnemonic(wallet.mnemonic)
    }
    
    func createWalletMnemonic() -> String {
        return walletCore.createWalletMnemonic()
    }
    
    func createBTCAddressFromMnemonic(_ mnemonic: String) -> String {
        return walletCore.createBTCAddressFromMnemonic(mnemonic)
    }
    
    func createWalletWithName(_ name: String, mnemonic: [String]) {
        var walletInfo = WalletInfo(mnemonic: mnemonic.joined(separator: " "), name: name)
        walletInfo.address = createBTCAddressFromMnemonic(walletInfo.mnemonic)
        DefaultStore().storeWallet(wallet: walletInfo)
    }
    
    func getWallet() -> WalletInfo? {
        let wallet =  DefaultStore().loadWallet()
        return wallet
    }
}


