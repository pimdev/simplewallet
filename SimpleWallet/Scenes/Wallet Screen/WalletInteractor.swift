//
//  WalletInteractor.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

protocol WalletBusinessLogic {
    func loadWallet()
    func copyWalletAddress()
}

class WalletInteractor {
    var presenter: WalletPresentationLogic?
    var worker: WalletWorker = WalletWorker()
}

extension WalletInteractor: WalletBusinessLogic {
    func loadWallet() {
        presenter?.presentWallet(worker.getWallet())
    }
    
    func copyWalletAddress() {
        presenter?.presentWalletAddress(worker.walletAddress)        
    }
}
