//
//  DashboardWorker.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

class DashboardWorker {
    
    func createActions() -> [Action] {
        return [Action(actionType: .wallets), Action(actionType: .transactions)]
    }
}
