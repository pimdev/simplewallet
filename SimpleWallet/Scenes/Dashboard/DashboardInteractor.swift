//
//  DashboardInteractor.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

protocol DashboardBusinessLogic {
    func initiateSetup()
    func handleAction(_ action: Action)
}

class DashboardInteractor {
    var presenter: (DashboardPresentationLogic & DashboardActionPresentationLogic)?
    var worker: DashboardWorker?
    
    init() {
        worker = DashboardWorker()
    }
}

extension DashboardInteractor: DashboardBusinessLogic {
    func initiateSetup() {
        if let actions = worker?.createActions() {
            presenter?.didLoadActions(actions)
        }
    }
    
    func handleAction(_ action: Action) {
        switch action.actionType {
        case .wallets:
            presenter?.openWallet()
        case .transactions:
            presenter?.transferTokens()
        default:
            break;
        }
    }
}
