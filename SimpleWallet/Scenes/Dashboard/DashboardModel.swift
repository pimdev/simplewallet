//
//  DashboardModel.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

enum ActionType: Int, Hashable, Equatable {
    case wallets
    case transactions
    
    case noAction = 999
    
    
    var name: String {
        switch self {
        case .wallets:
            return "Show My Wallet"
        case .transactions:
            return "Transaction"
        default:
            return "No Action"
        }
    }
}

extension Int {
    var actionType: ActionType {
        ActionType(rawValue: self) ?? ActionType(rawValue: ActionType.noAction.rawValue)!
    }
}

class Action {
    var id: Int {
        return actionType.rawValue
    }
    
    var name: String {
        return actionType.name
    }
    
    let actionType: ActionType
    
    init(actionType: ActionType) {
        self.actionType = actionType
    }
}

extension Action: Equatable, Hashable {
    static func == (lhs: Action, rhs: Action) -> Bool {
        lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
