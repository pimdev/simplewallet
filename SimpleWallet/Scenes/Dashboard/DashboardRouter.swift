//
//  DashboardRouter.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import UIKit

protocol DashboardRoutingLogic {
    func openWalletScreen()
    func openTransferTokensScreen()
}
protocol DashboardDataPassing {}

class DashboardRouter: DashboardDataPassing {
    weak var viewController: DashboardViewController?
}

extension DashboardRouter: DashboardRoutingLogic {
    func openWalletScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let walletsViewController = storyboard.instantiateViewController(identifier: WalletViewController.identifier)
        viewController?.navigationController?.pushViewController(walletsViewController, animated: true)
    }
    
    func openTransferTokensScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let transactionsViewController = storyboard.instantiateViewController(identifier: TransactionsViewController.identifier)
        viewController?.present(transactionsViewController, animated: true, completion: nil)
    }
}
