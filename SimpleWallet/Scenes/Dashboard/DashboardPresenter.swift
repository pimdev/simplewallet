//
//  DashboardPresenter.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

protocol DashboardPresentationLogic: class {
    func didLoadActions(_ actions: [Action])
}

protocol DashboardActionPresentationLogic: class {
    func openWallet()
    func transferTokens()
}

class DashboardPresenter {
    weak var viewController: (DashboardDisplayLogic & DashboardActionPresentationLogic)? {
        didSet {
            viewController?.updateNavigationTitle("Simple Wallet")
        }
    }
}

extension DashboardPresenter: DashboardPresentationLogic {
    func didLoadActions(_ actions: [Action]) {
        viewController?.didUpdateDefaultActions(actions)
    }
}

extension DashboardPresenter: DashboardActionPresentationLogic {
    func openWallet() {
        viewController?.openWallet()
    }
    
    func transferTokens() {
        viewController?.transferTokens()
    }
}
