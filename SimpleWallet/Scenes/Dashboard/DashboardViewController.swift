//
//  DashboardViewController.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import UIKit

enum Section {
    case main
}

protocol DashboardDisplayLogic: class {
    func didUpdateDefaultActions(_ actions: [Action])
    func updateNavigationTitle(_ title: String)
}

protocol DashboardActionDisplayLogic: class {
    func openWallet()
    func transferTokens()
}

class DashboardViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    private lazy var dataSource = makeDataSource()
    
    var router: DashboardRouter?
    var interactor: DashboardInteractor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup() {
        setupCollectionView()
        
        let viewController = self
        
        let router = DashboardRouter()
        let interactor = DashboardInteractor()
        let presenter = DashboardPresenter()
        
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        
        setupActions()
    }
    
    func setupActions() {
        interactor?.initiateSetup()
    }
}

// MARK: Helpers

extension DashboardViewController {
    func setupCollectionView() {
        collectionView.collectionViewLayout = makeListLayout()
        collectionView.dataSource = makeDataSource()
        collectionView.delegate = self
    }
}

// MARK: DashboardDisplayLogic

extension DashboardViewController: DashboardDisplayLogic {
    func didUpdateDefaultActions(_ actions: [Action]) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Action>()
        
        snapshot.appendSections([.main])
        snapshot.appendItems(actions, toSection: .main)

        dataSource.apply(snapshot)
    }
    
    func updateNavigationTitle(_ title: String) {
        self.title = title
    }
}

extension DashboardViewController: DashboardActionPresentationLogic {
    func openWallet() {
        router?.openWalletScreen()
    }
    
    func transferTokens() {
        router?.openTransferTokensScreen()
    }
}

// MARK: Collection View Delegate

extension DashboardViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let action = dataSource.itemIdentifier(for: indexPath) else {   return}
        interactor?.handleAction(action)
    }
}

// MARK: Collection View Data Source

extension DashboardViewController {
    private static let cellReuseID = "action-cell"
    
    func makeDataSource() -> UICollectionViewDiffableDataSource<Section, Action> {
        UICollectionViewDiffableDataSource(
            collectionView: collectionView,
            cellProvider: { collectionView, indexPath, action in
                let cell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: Self.cellReuseID,
                    for: indexPath
                ) as! DashboardCollectionViewCell
                
                cell.textLabel?.text = action.name
                cell.styleAsButton()
                
                return cell
            }
        )
    }
}


