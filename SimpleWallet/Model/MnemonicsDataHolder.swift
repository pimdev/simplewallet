//
//  MnemonicDataHolder.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 04.02.2021.
//

import Foundation

class MnemonicDataValidator {
    var data: [String]? {
        didSet {
            self.shuffledData = data?.shuffled()
        }
    }
    var shuffledData: [String]?
    var dataToConfirm: [String]?
    var validatingData: [Int] = []
    
    var isValidatingData: Bool {
        self.data != nil
    }
    
    var validatedData: [String] {
        return validatingData.map { index in
            return self.shuffledData?[index] ?? ""
        }
    }
    
    func toggleIndex(_ index: Int) {
        if validatingData.contains(index) {
            validatingData.removeAll(where: { $0 == index })
        } else {
            validatingData.append(index)
        }
    }
    
    var selectionIsValid: Bool {
        return data == validatedData
    }
}
