//
//  WalletLogic.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 05.02.2021.
//

import Foundation
import WalletCore

protocol WalletLogic {
    func createWalletMnemonic() -> String
    func createBTCAddressFromMnemonic(_ mnemonic: String) -> String
}

class WalletCoreLogic: WalletLogic {
    func createWalletMnemonic() -> String {
        let wallet = HDWallet(strength: 256, passphrase: "")
        return wallet.mnemonic
    }
    
    func createBTCAddressFromMnemonic(_ mnemonic: String) -> String {
        let wallet = HDWallet(mnemonic: mnemonic, passphrase: "")
        let addressBTC = wallet.getAddressForCoin(coin: .bitcoin)
        return addressBTC
    }
}
