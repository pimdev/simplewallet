//
//  WalletStore.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 03.02.2021.
//

import Foundation

struct WalletInfo {
    var mnemonic: String
    var name: String
    
    var address: String?
}

protocol WalletStore {
    func storeWallet(wallet: WalletInfo)
    func loadWallet() -> WalletInfo?
}

extension WalletInfo: Hashable, Equatable {}

class DefaultStore: WalletStore {
    private static let mnemonicKey: String = "Mnemonic"
    
    func storeWallet(wallet: WalletInfo) {
        UserDefaults.standard.setValue(wallet.dictionary, forKey: DefaultStore.mnemonicKey)
    }
    
    func loadWallet() -> WalletInfo? {
        return (UserDefaults.standard.object(forKey: DefaultStore.mnemonicKey) as? [String: String])?.walletInfo
    }
}

extension WalletInfo {
    fileprivate static let wordsKey: String = "Words"
    fileprivate static let walletNameKey: String = "Wallet"
    fileprivate static let walletAddressKey: String = "Address"
    
    var dictionary: [String: String?] {
        return [WalletInfo.wordsKey:self.mnemonic, WalletInfo.walletNameKey:self.name,WalletInfo.walletAddressKey:self.address]
    }
}

extension Dictionary {
    var walletInfo: WalletInfo? {
        guard let words = self[WalletInfo.wordsKey as! Key] as? String, let name = self[WalletInfo.walletNameKey as! Key] as? String else { return nil }
        var walletInfo = WalletInfo(mnemonic: words, name: name)
        walletInfo.address = self[WalletInfo.walletAddressKey as! Key] as? String
        return walletInfo
    }
}

