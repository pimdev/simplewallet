//
//  CryptoService.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 05.02.2021.
//

import Foundation
import Moya

enum CryptoService {
    case transactions
    case addressInfo(address: String)
}

extension CryptoService: TargetType {
    var baseURL: URL { return URL(string: "https://api.blockchair.com/bitcoin/testnet")! }
    var path: String {
        switch self {
        case .transactions:
            return "/transactions"
        case let .addressInfo(address):
            return "/dashboards/addresses/\(address)"
        }
    }
    var method: Moya.Method {
        switch self {
        case .transactions, .addressInfo:
            return .get
        }
    }
    var task: Task {
        switch self {
        case .transactions, .addressInfo:
            return .requestPlain
        }
    }
    var sampleData: Data {
        switch self {
        case .transactions:
            return "Bitcoin transactions".utf8Encoded
        case .addressInfo:
            return "Bitcoint testnet address info".utf8Encoded
        }
    }
    var headers: [String : String]? {
        return nil
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
