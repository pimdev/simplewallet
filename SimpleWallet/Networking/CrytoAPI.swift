//
//  CrytoAPI.swift
//  SimpleWallet
//
//  Created by Mihail-Ioan Popa on 05.02.2021.
//

import Foundation
import Moya

struct AddressDashboard: Codable {
    struct AddressData: Codable {
        enum CodingKeys: String, CodingKey {
            case set
            case transactions
            case transactionsInfo = "utxo"
        }
        
        struct AddressSet: Codable {
            enum CodingKeys: String, CodingKey {
                case count = "address_count"
            }
            var count: Int
        }
        struct TransactionInfo: Codable {
            enum CodingKeys: String, CodingKey {
                case hash = "transaction_hash"
                case value
                case address
            }
            var hash: String
            var value: Float
            var address: String
            
        }
        var set: AddressDashboard.AddressData.AddressSet
        var transactions: [String]
        var transactionsInfo: [TransactionInfo]
    }
    var data: AddressDashboard.AddressData
}

extension AddressDashboard.AddressData.TransactionInfo: Equatable, Hashable {}

enum BlockchairError: Error {
    case invalidJSON
}

protocol TransactionAPI {
    func getTransactionsWith(completion: @escaping (Result<String, Error>) -> ())
    func getAddressInfoFor(address: String, completion: @escaping (Result<AddressDashboard, Error>) -> ())
}

class BlockchairTransactionAPI: TransactionAPI {
    let provider = MoyaProvider<CryptoService>()
    
    func getTransactionsWith(completion: @escaping (Result<String, Error>) -> ()) {
        provider.request(.transactions) { result in
            switch result {
            case let .success(response):
                let data = try? response.mapJSON()
                completion(.success(data.debugDescription))
                break
            case let .failure(error):
                print(error)
                break
            }
        }
    }
    
    func getAddressInfoFor(address: String, completion: @escaping (Result<AddressDashboard, Error>) -> ()) {
        provider.request(.addressInfo(address: address)) { result in
            switch result {
            case let .success(response):
                let decoder = JSONDecoder()
                do {
                    let decoded = try decoder.decode(AddressDashboard.self, from: response.data)
                    completion(.success(decoded))
                } catch {
                    completion(.failure(BlockchairError.invalidJSON))
                    print("Failed to decode JSON")
                }
                break
            case let .failure(error):
                print(error)
                break
            }
        }
    }
}
